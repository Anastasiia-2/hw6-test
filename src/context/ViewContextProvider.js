import { createContext, useState } from "react";

export const ViewContext = createContext(null);

export const ViewContextProvider = ({ children }) => {
  const [view, setView] = useState("cards");

  const changeView = () => {
    setView(prev => (prev === "cards" ? "line" : "cards"));
  };

  const value = {
    view,
    changeView,
  };

  return <ViewContext.Provider value={value}>{children}</ViewContext.Provider>;
};

export default ViewContextProvider;
