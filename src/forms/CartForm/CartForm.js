import React from "react";
import { Formik, Form } from "formik";
import CustomInput from "../../components/CustomInput/CustomInput";
import validationSchema from "./validationSchema";
import styles from "./CartForm.module.scss";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { resetCartItems } from "../../redux/cart/actionCreators";
import { PatternFormat } from "react-number-format";

export default function CartForm() {
  console.log(
    <PatternFormat format="+1 (###) #### ###" allowEmptyFormatting mask="_" />
  );

  const initialValues = {
    name: "",
    surname: "",
    age: "",
    deliveryAddress: "",
    mobileNumber: "",
  };
  const cartItems = useSelector(state => state.cart.cartItems, shallowEqual);
  const dispatch = useDispatch();

  const onSubmit = (values, actions) => {
    const newCartItems = cartItems.map(item => {
      return {
        item: item.name,
        count: item.count,
        price: item.price,
      };
    });

    const totalPrice = newCartItems.reduce((sum, item) => {
      const price = Number([...item.price].splice(1).join(""));
      return (sum += price * item.count);
    }, 0);

    console.log(values);
    console.log("You buy:", ...newCartItems);
    console.log("Total price:", totalPrice);
    dispatch(resetCartItems());
    actions.resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {form => {
        return (
          <Form className={styles.form}>
            <h3 className={styles.title}>Make an order</h3>
            <CustomInput name="name" id="name" label="First Name:" />
            <CustomInput name="surname" id="surname" label="Last Name:" />
            <CustomInput name="age" id="age" label="Age:" />
            <CustomInput
              name="deliveryAddress"
              id="deliveryAddress"
              label="Delivery address:"
            />
            <CustomInput
              name="mobileNumber"
              id="mobileNumber"
              label="Mobile number:"
            />

            <button
              type="submit"
              className={styles.button}
              disabled={!form.isValid}
            >
              Order
            </button>
          </Form>
        );
      }}
    </Formik>
  );
}
