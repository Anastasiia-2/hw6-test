import {
  TOGGLE_MODAL,
  SET_MODAL_HEADER,
  SET_MODAL_TEXT,
  SET_MODAL_CALLBACK,
} from "./action";

export const toggleModal = () => {
  return { type: TOGGLE_MODAL };
};

export const setModalHeader = text => {
  return { type: SET_MODAL_HEADER, payload: text };
};

export const setModalText = text => {
  return { type: SET_MODAL_TEXT, payload: text };
};

export const setModalCallback = callback => {
  return { type: SET_MODAL_CALLBACK, payload: callback };
};
