import { modalReducer, initialState } from "./reducer";
import {
  toggleModal,
  setModalHeader,
  setModalText,
  setModalCallback,
} from "./actionCreators";

describe("modal reducers", () => {
  test("should handle TOGGLE_MODAL action", () => {
    const newState = modalReducer(initialState, toggleModal());
    expect(newState.isModalOpen).toBe(true);
  });

  test("should handle SET_MODAL_HEADER action", () => {
    const modalHeader = "Modal header";
    const newState = modalReducer(initialState, setModalHeader(modalHeader));
    expect(newState.header).toBe(modalHeader);
  });

  test("should handle SET_MODAL_TEXT action", () => {
    const modalText = "Some text";
    const newState = modalReducer(initialState, setModalText(modalText));
    expect(newState.text).toBe(modalText);
  });

  test("should handle SET_MODAL_CALLBACK action", () => {
    const modalCallback = () => {};
    const newState = modalReducer(
      initialState,
      setModalCallback(modalCallback)
    );
    expect(newState.callback).toEqual(modalCallback);
  });

  test("should return the initial state for an unknown action type", () => {
    const initialState = {
      isModalOpen: false,
      header: "",
      text: "",
      callback: () => {},
    };

    const newState = modalReducer(initialState, { type: undefined });
    expect(newState).toEqual(initialState);
  });

  test("should return the initial state without action", () => {
    const initialState = {
      isModalOpen: false,
      header: "",
      text: "",
      callback: () => {},
    };
    const newState = modalReducer(initialState, undefined);
    expect(newState).toEqual(initialState);
  });
});
