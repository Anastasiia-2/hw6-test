import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { itemsReducer } from "./items/reducer";
import { modalReducer } from "./modal/reducer";
import { cartReducer } from "./cart/reducer";

const rootReducer = combineReducers({
  items: itemsReducer,
  modal: modalReducer,
  cart: cartReducer,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
