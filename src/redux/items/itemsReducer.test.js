import { SET_FAVOURITES, SET_ITEMS } from "./action";
import { itemsReducer } from "./reducer";

const initialState = {
  allItems: [],
  favItems: [],
};

describe("itemsReducer", () => {
  test("should handle SET_ITEMS action", () => {
    const payloadItemsMock = [
      { id: 1, name: "Item 1" },
      { id: 2, name: "Item 2" },
    ];
    const newState = itemsReducer(initialState, {
      type: SET_ITEMS,
      payload: payloadItemsMock,
    });
    expect(newState.allItems).toEqual(payloadItemsMock);
  });

  test("should handle SET_FAVOURITES action", () => {
    const payloadFavItemsMock = [
      { id: 1, name: "Item 1" },
      { id: 2, name: "Item 2" },
    ];
    const newState = itemsReducer(initialState, {
      type: SET_FAVOURITES,
      payload: payloadFavItemsMock,
    });
    expect(newState.favItems).toEqual(payloadFavItemsMock);
  });

  test("should handle TOGGLE_FAVOURITE_ITEM action", () => {
    const payloadFavItemMock = { id: 1, name: "Item 1" };

    const newState = itemsReducer(initialState, {
      type: SET_FAVOURITES,
      payload: payloadFavItemMock,
    });
    expect(newState.favItems).toEqual(payloadFavItemMock);
  });

  test("should return the initial state for an unknown action type", () => {
    const newState = itemsReducer(initialState, { type: undefined });
    expect(newState).toEqual(initialState);
  });

  test("should return the initial state without action", () => {
    const newState = itemsReducer(initialState, undefined);
    expect(newState).toEqual(initialState);
  });
});
