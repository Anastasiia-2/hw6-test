import { FAVOR } from "../../constants";
import { getStateFromLS } from "../../utils";
import { SET_ITEMS, SET_FAVOURITES, TOGGLE_FAVOURITE_ITEM } from "./action";
import axios from "axios";

export const setItems = items => {
  return { type: SET_ITEMS, payload: items };
};

export const setFavourites = items => {
  return {
    type: SET_FAVOURITES,
    payload: items,
  };
};

export const toggleFavouriteItem = item => {
  return {
    type: TOGGLE_FAVOURITE_ITEM,
    payload: item,
  };
};

export const fetchItems = () => {
  return async dispatch => {
    try {
      const { data } = await axios.get("http://localhost:5000/api/products");
      dispatch(setItems(data));
    } catch (err) {
      console.log(err);
      try {
        const { data } = await axios.get("./itemsList.json");
        dispatch(setItems(data));
      } catch (data) {
        console.log("Ошибка при загрузке моковых данных:", data);
      }
    }
  };
};

export const fetchFavourites = () => {
  return dispatch => {
    const items = getStateFromLS(FAVOR) ? getStateFromLS(FAVOR) : [];
    dispatch(setFavourites(items));
  };
};
