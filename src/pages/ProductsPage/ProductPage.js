import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import styles from "./ProductPage.module.scss";
import { shallowEqual, useSelector } from "react-redux";

export default function ProductPage() {
  const items = useSelector(state => state.items.allItems, shallowEqual);
  return (
    <section>
      <h2 className={styles.title}>Our products</h2>
      <ItemsContainer items={items} />
    </section>
  );
}
