import React from "react";
import { useSelector } from "react-redux";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

export default function FavoritePage() {
  const newItems = useSelector(state => state.items.favItems);

  return newItems?.length ? (
    <ItemsContainer items={newItems} />
  ) : (
    <p>"You have not yet added any product to your favorites."</p>
  );
}
