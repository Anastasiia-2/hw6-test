import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import { shallowEqual, useSelector } from "react-redux";
import CartForm from "../../forms/CartForm/CartForm";

import styles from "./CartPage.module.scss";

export default function CartPage() {
  const cartItems = useSelector(state => state.cart.cartItems, shallowEqual);

  return cartItems.length ? (
    <div className={styles.root}>
      <ItemsContainer items={cartItems} />
      <CartForm />
    </div>
  ) : (
    <p style={styles.paragraph}>
      "You have not added anything to your cart yet..."
    </p>
  );
}
