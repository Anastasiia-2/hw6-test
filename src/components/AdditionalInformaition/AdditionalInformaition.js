import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { AiOutlineHeart } from "react-icons/ai";
import { BsCart } from "react-icons/bs";
import { TfiViewListAlt } from "react-icons/tfi";
import { TfiViewGrid } from "react-icons/tfi";
import styles from "./Additional.module.scss";
import { ViewContext } from "../../context/ViewContextProvider";
import classNames from "classnames";
import { NavLink } from "react-router-dom";

export default function AdditionalInformaition() {
  const { view, changeView } = useContext(ViewContext);

  const favCount = useSelector(state => state.items.favItems?.length) || 0;
  const cartCount = useSelector(state => state.cart.fullAmount);

  return (
    <div className={styles.additionalWrapper}>
      <NavLink to="/cart" className={styles.actions}>
        <BsCart size="24" />
        <span className={styles.actionsCount}>{cartCount}</span>
      </NavLink>

      <NavLink to="/favorite" className={styles.actions}>
        <AiOutlineHeart size="24" />
        <span className={styles.actionsCount}>{favCount}</span>
      </NavLink>

      <button
        onClick={changeView}
        className={classNames(styles.actions, styles.button)}
      >
        {view === "cards" ? (
          <TfiViewListAlt size="24" />
        ) : (
          <TfiViewGrid size="24" />
        )}
      </button>
    </div>
  );
}
