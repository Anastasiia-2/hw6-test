import { fireEvent, render, screen } from "@testing-library/react";
import Modal from "./Modal";
import { useDispatch, useSelector } from "react-redux";
import { toggleModal } from "../../redux/modal/actionCreators";

jest.mock("react-redux");

describe("Modal buttons", () => {
  test("should call confirm fn", () => {
    const fnConfirm = jest.fn();
    useSelector
      .mockReturnValueOnce("Header")
      .mockReturnValueOnce("Text")
      .mockReturnValueOnce(fnConfirm);
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);
    render(<Modal />);
    const confirmBtn = screen.getByText("Yes");
    fireEvent.click(confirmBtn);
    expect(fnConfirm).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(toggleModal());
  });

  test("should close modal", () => {
    useSelector.mockReturnValueOnce("Header").mockReturnValueOnce("Text");
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);
    render(<Modal />);
    const cancelBtn = screen.getByText("No");
    fireEvent.click(cancelBtn);
    expect(dispatch).toHaveBeenCalledWith(toggleModal());
    const closeBtn = screen.getByTestId("closebtn-test");
    fireEvent.click(closeBtn);
    expect(dispatch).toHaveBeenCalledWith(toggleModal());
  });

  test("should close modal with click by div", () => {
    useSelector.mockReturnValueOnce("Header").mockReturnValueOnce("Text");
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);
    render(<Modal />);

    const wrapperElement = screen.getByTestId("test_wrapper");
    fireEvent.click(wrapperElement);
    expect(dispatch).toHaveBeenCalledWith({ type: "TOGGLE_MODAL" });
  });
});
