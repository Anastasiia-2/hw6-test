import React, { memo } from "react";
import PropTypes from "prop-types";
import { useField } from "formik";
import styles from "./CustomInput.module.scss";

function CustomInput({ label, type, ...props }) {
  const [field, meta] = useField(props.name);

  return (
    <>
      <label htmlFor={props.id} className={styles.label}>
        {label}
        {meta.touched && meta.error ? (
          <div className={styles.error}>{meta.error}</div>
        ) : null}
      </label>
      <input type={type} {...field} {...props} className={styles.input} />
    </>
  );
}

CustomInput.propTypes = {
  type: PropTypes.string,
};
CustomInput.default = {
  type: "text",
};

export default memo(CustomInput);
