import { render, screen } from "@testing-library/react";
import Logo from "./Logo";

describe("Logo", () => {
  test("should render", () => {
    const { asFragment } = render(<Logo />);
    screen.debug();
    expect(asFragment()).toMatchSnapshot();
  });

  test("should contain an element with text Furniking ", () => {
    render(<Logo />);
    const logoText = screen.getByText("Furniking");
    expect(logoText).toBeInTheDocument();
  });
});
