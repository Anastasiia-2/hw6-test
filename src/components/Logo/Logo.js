import React from "react";
import { MdChair } from "react-icons/md";
import styles from "./Logo.module.scss";

function Logo() {
  return (
    <a href="/" data-testid="test-logo" className={styles.logo}>
      <span>{<MdChair />}</span>
      Furniking
    </a>
  );
}

export default Logo;
