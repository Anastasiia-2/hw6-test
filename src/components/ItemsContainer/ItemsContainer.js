import React, { useContext } from "react";
import classNames from "classnames";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";
import { ViewContext } from "../../context/ViewContextProvider";

export default function ItemsContainer({ items }) {
  const { view } = useContext(ViewContext);
  console.log("from items container: ", view);

  return (
    <ul className={classNames(styles.list, { [styles.line]: view === "line" })}>
      {items?.map(item => {
        return (
          <li key={item?.id} className={styles.item}>
            <Item item={item} />
          </li>
        );
      })}
    </ul>
  );
}
