import { render } from "@testing-library/react";
import PageNavigation from "./PageNavigation";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  NavLink: jest.fn(({ to, children }) => <a href={to}>{children}</a>),
}));

describe("PageNavigation", () => {
  test("should render", () => {
    const { asFragment } = render(<PageNavigation />);
    expect(asFragment()).toMatchSnapshot();
  });
});
