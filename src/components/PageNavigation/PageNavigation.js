import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./PageNavigation.module.scss";

export default function PageNavigation() {
  return (
    <nav className={styles.container}>
      <ul className={styles.list}>
        <li className={styles.item}>
          <NavLink
            to="/"
            className={({ isActive }) => (isActive ? styles.active : "")}
          >
            Products
          </NavLink>
        </li>
        <li className={styles.item}>
          <NavLink
            to="/cart"
            className={({ isActive }) => (isActive ? styles.active : "")}
          >
            Cart
          </NavLink>
        </li>
        <li className={styles.item}>
          <NavLink
            to="/favorite"
            className={({ isActive }) => (isActive ? styles.active : "")}
          >
            Favorites
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}
