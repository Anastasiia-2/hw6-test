import React from "react";
import { Routes, Route } from "react-router-dom";
import ProductPage from "./pages/ProductsPage/ProductPage";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import NotFound from "./pages/NotFound/NotFound";

export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<ProductPage />} />
      <Route path="/cart" element={<CartPage />} />
      <Route path="/favorite" element={<FavoritePage />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}
